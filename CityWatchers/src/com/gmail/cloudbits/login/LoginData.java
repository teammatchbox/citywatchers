package com.gmail.cloudbits.login;

public class LoginData {

	private String userEmailAddress;
	private String userPassword;

	public LoginData() {}

	public LoginData(String userEmailAddress, String userPassword) {
		this.userEmailAddress = userEmailAddress;
		this.userPassword = userPassword;
	}

	public String getUserEmailAddress() {
		return this.userEmailAddress;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

}