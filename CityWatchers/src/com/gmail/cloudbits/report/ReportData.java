package com.gmail.cloudbits.report;

import com.gmail.cloudbits.auth.AuthToken;

public class ReportData {

	private AuthToken authToken;
	private String reportAddress; 
	private String reportPriority;
	private String reportType;
	private String reportDescription;

	/**
	 * Empty constructor to handle error situations.
	 */
	public ReportData() {}

	public ReportData (AuthToken authToken, String reportAddress, String reportPriority, String reportType, String reportDescription) {
		this.authToken = authToken;
		this.reportAddress = reportAddress;
		this.reportPriority = reportPriority;
		this.reportType = reportType;
		this.reportDescription = reportDescription;
	}

	/**
	 * @return the authToken
	 */
	public final AuthToken getAuthToken() {
		return authToken;
	}

	/**
	 * @return the reportAddress
	 */
	public final String getReportAddress() {
		return reportAddress;
	}

	/**
	 * @return the reportPriority
	 */
	public final String getReportPriority() {
		return reportPriority;
	}

	/**
	 * @return the reportType
	 */
	public final String getReportType() {
		return reportType;
	}

	/**
	 * @return the reportDescription
	 */
	public final String getReportDescription() {
		return reportDescription;
	}

	
	
}