package com.gmail.cloudbits.report;

import com.google.appengine.api.datastore.Entity;

public class ReportDataUtil {

	private static final String REPORT_ENTITY_TABLE_NAME = "Reports";
	private static final String REPORT_CREATION_TIME = "report_creation_time";
	private static final String REPORT_ADDRESS = "report_address";
	private static final String REPORT_PRIORITY = "report_priority";
	private static final String REPORT_TYPE = "report_type";
	private static final String REPORT_DESCRIPTION = "report_description";
	private static final String REPORT_SUBMITTED_BY = "report_submitted_by";
	private static final String REPORT_CURRENT_STATUS = "report_current_status";

	private ReportDataUtil() {
	}

	public static final Entity createReportEntity(String reportAddress, String reportPriority, String reportType,
			String reportDescription, String watcherSubmiter, String submittedReport) {
		Entity reportEntity = new Entity(REPORT_ENTITY_TABLE_NAME);
		reportEntity.setProperty(REPORT_ADDRESS, reportAddress);
		reportEntity.setProperty(REPORT_PRIORITY, reportPriority);
		reportEntity.setProperty(REPORT_TYPE, reportType);
		reportEntity.setProperty(REPORT_DESCRIPTION, reportDescription);
		reportEntity.setProperty(REPORT_SUBMITTED_BY, watcherSubmiter);
		reportEntity.setProperty(REPORT_CURRENT_STATUS, submittedReport);
		reportEntity.setProperty(REPORT_CREATION_TIME, System.currentTimeMillis());
		return reportEntity;
	}
}