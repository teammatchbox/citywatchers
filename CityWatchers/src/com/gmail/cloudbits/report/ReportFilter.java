package com.gmail.cloudbits.report;

public class ReportFilter {

	private boolean getSubmitted;
	private boolean getOpened;
	private boolean getClosed;

	public ReportFilter() {}

	public ReportFilter(boolean getSubmitted, boolean getOpened, boolean getClosed) {
		this.getSubmitted = getSubmitted;
		this.getOpened = getOpened;
		this.getClosed = getClosed;
	}

	/**
	 * @return the getSubmitted
	 */
	public final boolean isGetSubmitted() {
		return getSubmitted;
	}

	/**
	 * @return the getOpened
	 */
	public final boolean isGetOpened() {
		return getOpened;
	}

	/**
	 * @return the getClosed
	 */
	public final boolean isGetClosed() {
		return getClosed;
	}

}
