package com.gmail.cloudbits.fixer;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PreparedQuery.TooManyResultsException;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;

public final class FixerDataUtil {

	// Table information
	private static final String FIXER_ENTITY_TABLE_NAME = "Users";
	private static final String FIXER_TYPE = "user_type";
	private static final String FIXER_PASSWORD_SECRET = "user_password_secret";
	private static final String FIXER_CREATION_TIME = "user_creation_time";

	// Account information
	private static final String FIXER_ACCOUNT_ID = "user_account_id";
	private static final String FIXER_ACCOUNT_PASSWORD = "user_account_password";

	// Employee information
	private static final String FIXER_EMPLOYEE_ENTITY = "user_employee_entity";

	// Employer information
	private static final String FIXER_EMPLOYER_ENTITY = "user_employer_entity";
	private static final String FIXER_REPORT_SUPERTYPE = "user_report_supertype";
	private static final String FIXER_REPORT_SUBTYPE = "user_report_subtype";

	// Assigned location information
	private static final String FIXER_ASSIGNED_DISTRICT_AREA = "user_assigned_district_area";
	private static final String FIXER_ASSIGNED_TOWNHALL_AREA = "user_assigned_townhall_area";
	private static final String FIXER_ASSIGNED_NEIGHBORHOOD_AREA = "user_assigned_neighborhood_area";

	/**
	 * Just for class clarity.
	 */
	private FixerDataUtil() {
	}

	public static final boolean isAccountIDTaken(DatastoreService datastore, Transaction txn, String accountID)
			throws IllegalStateException, TooManyResultsException {
		Filter accountIDFilter = new FilterPredicate(FIXER_ACCOUNT_ID, FilterOperator.EQUAL, accountID);
		Query accountIDQuery = new Query(FIXER_ENTITY_TABLE_NAME).setKeysOnly().setFilter(accountIDFilter);
		PreparedQuery q = datastore.prepare(accountIDQuery);
		Entity accountIDKey = q.asSingleEntity();
		if (accountIDKey == null) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean validFixerDataFields(String fixerAccountPassword, String fixerAccountPasswordCheck,
			String fixerEmployeeEntity, String fixerReportSuperType, String fixerReportSubType,
			String fixerDistrictArea, String fixerTownHallArea, String fixerNeighborhoodArea) {
		return false;
	}

	public static Entity createFixerEntity(String accountID, String fixerType, String fixerPasswordSecret,
			String accountPassword, String fixerEmployeeEntity, String fixerEmployerEntity, String fixerReportSupertype,
			String fixerReportSubtype, String fixerAssignedDistrictArea, String fixerAssignedTownHallArea,
			String fixerAssignedNeighborhoodArea) {
		Entity fixerEntity = new Entity(FIXER_ENTITY_TABLE_NAME, accountID);
		fixerEntity.setIndexedProperty(FIXER_TYPE, fixerType);
		fixerEntity.setIndexedProperty(FIXER_PASSWORD_SECRET, fixerPasswordSecret);
		fixerEntity.setIndexedProperty(FIXER_CREATION_TIME, System.currentTimeMillis());
		fixerEntity.setIndexedProperty(FIXER_ACCOUNT_ID, accountID);
		fixerEntity.setIndexedProperty(FIXER_ACCOUNT_PASSWORD,
				DigestUtils.sha512Hex(accountPassword + fixerPasswordSecret));
		fixerEntity.setIndexedProperty(FIXER_EMPLOYEE_ENTITY, fixerEmployeeEntity);
		fixerEntity.setIndexedProperty(FIXER_EMPLOYER_ENTITY, fixerEmployerEntity);
		fixerEntity.setIndexedProperty(FIXER_REPORT_SUPERTYPE, fixerReportSupertype);
		fixerEntity.setIndexedProperty(FIXER_REPORT_SUBTYPE, fixerReportSubtype);
		fixerEntity.setIndexedProperty(FIXER_ASSIGNED_DISTRICT_AREA, fixerAssignedDistrictArea);
		fixerEntity.setIndexedProperty(FIXER_ASSIGNED_TOWNHALL_AREA, fixerAssignedTownHallArea);
		fixerEntity.setIndexedProperty(FIXER_ASSIGNED_NEIGHBORHOOD_AREA, fixerAssignedNeighborhoodArea);
		return fixerEntity;
	}

}