package com.gmail.cloudbits.fixer;

public class FixerData {

	// Account information (Uniquely identify the fixer account).
	private String accountID; // Unique camp between fixers, probably a number
	// to identify them.
	private String accountPassword;
	private String accountPasswordCheck;

	// Employee information
	private String fixerEmployeeEntity;

	// Employer information
	private String fixerReportSuperType; // Assigned to deal with water types
	// for example
	private String fixerReportSubType; // Then here, we can define which types
	// of water reports
	private String fixerEmployerEntity;
	private String fixerDistrictArea;
	private String fixerTownHallArea;
	private String fixerNeighborhoodArea;

	public FixerData() {
	}

	public FixerData(String accountID, String accountPassword, String accountPasswordCheck, String fixerEmployeeEntity,
			String fixerReportSuperType, String fixerReportSubType, String fixerEmployerEntity,
			String fixerDistrictArea, String fixerTownHallArea, String fixerNeighborhoodArea) {
		this.accountID = accountID;
		this.accountPassword = accountPassword;
		this.accountPasswordCheck = accountPasswordCheck;
		this.fixerEmployeeEntity = fixerEmployeeEntity;
		this.fixerReportSuperType = fixerReportSuperType;
		this.fixerReportSubType = fixerReportSubType;
		this.fixerEmployerEntity = fixerEmployerEntity;
		this.fixerDistrictArea = fixerDistrictArea;
		this.fixerTownHallArea = fixerTownHallArea;
		this.fixerNeighborhoodArea = fixerNeighborhoodArea;
	}

	/**
	 * @return the accountID
	 */
	public final String getAccountID() {
		return accountID;
	}

	/**
	 * @return the accountPassword
	 */
	public final String getAccountPassword() {
		return accountPassword;
	}

	/**
	 * @return the accountPasswordCheck
	 */
	public final String getAccountPasswordCheck() {
		return accountPasswordCheck;
	}

	/**
	 * @return the employeeEntity
	 */
	public final String getFixerEmployeeEntity() {
		return fixerEmployeeEntity;
	}

	/**
	 * @return the fixerReportSuperType
	 */
	public final String getFixerReportSuperType() {
		return fixerReportSuperType;
	}

	/**
	 * @return the fixerReportSubType
	 */
	public final String getFixerReportSubType() {
		return fixerReportSubType;
	}

	/**
	 * @return the fixerEmployerEntity
	 */
	public final String getFixerEmployerEntity() {
		return fixerEmployerEntity;
	}

	/**
	 * @return the fixerDistrictArea
	 */
	public final String getFixerDistrictArea() {
		return fixerDistrictArea;
	}

	/**
	 * @return the fixerTownHallArea
	 */
	public final String getFixerTownHallArea() {
		return fixerTownHallArea;
	}

	/**
	 * @return the fixerNeighborhoodArea
	 */
	public final String getFixerNeighborhoodArea() {
		return fixerNeighborhoodArea;
	}

}