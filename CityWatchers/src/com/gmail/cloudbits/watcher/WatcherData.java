package com.gmail.cloudbits.watcher;

/**
 * {@code WatcherData} is an unmodifiable class, that allows the unmarshalling
 * of a {@code WatcherData} object via {@link Json} and basic retrieval of its
 * object fields.
 * 
 * @author CloudBitsTeam
 */
public final class WatcherData {

	// Account information (Uniquely identify the watcher account).
	private byte[] accountProfilePhoto; // Optional, otherwise use default.
	private String accountID; // Unique camp between users, probably their email address.
	private String accountPassword;
	private String accountPasswordCheck;

	// Personal information (Welcome the watcher when he logs in, report creation and system statistics).
	private String watcherFirstname;
	private String watcherLastname;
	private String watcherBirthdate; // Date will be a String, so that it can be verified if it can be converted in a date object.
	private String watcherGender;
	private String watcherCellphone; // Optional.

	// Location information (Report creation, system statistics and search results retrieval).
	private String watcherDistrictArea;
	private String watcherTownHallArea;
	private String watcherNeighborhoodArea;

	/**
	 * Class clarity constructor.
	 */
	public WatcherData() {}

	/**
	 * Json unmarshalling constructor.
	 * 
	 * @param accountProfilePhoto
	 *            - The account profile photo.
	 * @param accountID
	 *            - The account id (Probably an email address).
	 * @param accountPassword
	 *            - The account password.
	 * @param accountPasswordCheck
	 *            - The account password check (Must match the account
	 *            password).
	 * @param watcherFirstname
	 *            - The watcher firstname.
	 * @param watcherLastname
	 *            - The watcher lastname.
	 * @param watcherBirthdate
	 *            The watcher birthdate.
	 * @param watcherGender
	 *            - The watcher gender.
	 * @param watcherCellphone
	 *            - The watcher cellphone contact.
	 * @param watcherDistrictArea
	 *            - The watcher district area.
	 * @param watcherTownHallArea
	 *            - The watcher townhall area.
	 * @param watcherNeighborhoodArea
	 *            - The watcher neighborhood area.
	 */
	public WatcherData(byte[] accountProfilePhoto, String accountID, String accountPassword, String accountPasswordCheck,
			String watcherFirstname, String watcherLastname, String watcherBirthdate, String watcherGender,
			String watcherCellphone, String watcherDistrictArea, String watcherTownHallArea, String watcherNeighborhoodArea) {
		this.accountProfilePhoto = accountProfilePhoto;
		this.accountID = accountID;
		this.accountPassword = accountPassword;
		this.accountPasswordCheck = accountPasswordCheck;
		this.watcherFirstname = watcherFirstname;
		this.watcherLastname = watcherLastname;
		this.watcherBirthdate = watcherBirthdate;
		this.watcherGender = watcherGender;
		this.watcherCellphone = watcherCellphone;
		this.watcherDistrictArea = watcherDistrictArea;
		this.watcherTownHallArea = watcherTownHallArea;
		this.watcherNeighborhoodArea = watcherNeighborhoodArea;
	}

	/**
	 * Return the account profile photo.
	 */
	public final byte[] getAccountProfilePhoto() {
		return this.accountProfilePhoto;
	}

	/**
	 * Return the account id.
	 */
	public final String getAccountID() {
		return this.accountID;
	}

	/**
	 * Return the account password.
	 */
	public final String getAccountPassword() {
		return this.accountPassword;
	}

	/**
	 * Return the account password check.
	 */
	public final String getAccountPasswordCheck() {
		return this.accountPasswordCheck;
	}

	/**
	 * Return the watcher firstname.
	 */
	public final String getWatcherFirstname() {
		return this.watcherFirstname;
	}

	/**
	 * Return the watcher lastname.
	 */
	public final String getWatcherLastname() {
		return this.watcherLastname;
	}

	/**
	 * Return the watcher birthdate.
	 */
	public final String getWatcherBirthdate() {
		return this.watcherBirthdate;
	}

	/**
	 * Return the watcher gender.
	 */
	public final String getWatcherGender() {
		return this.watcherGender;
	}

	/**
	 * Return the watcher cellphone contact.
	 */
	public final String getWatcherCellphone() {
		return this.watcherCellphone;
	}

	/**
	 * Return the watcher district area.
	 */
	public final String getWatcherDistrictArea() {
		return this.watcherDistrictArea;
	}

	/**
	 * Return the watcher townhall area.
	 */
	public final String getWatcherTownHallArea() {
		return this.watcherTownHallArea;
	}

	/**
	 * Return the watcher neighborhood area.
	 */
	public final String getWatcherNeighborhoodArea() {
		return this.watcherNeighborhoodArea;
	}

}