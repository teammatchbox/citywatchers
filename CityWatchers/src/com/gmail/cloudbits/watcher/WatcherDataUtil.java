package com.gmail.cloudbits.watcher;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.Channels;

import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.LocalDate;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PreparedQuery.TooManyResultsException;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;

public final class WatcherDataUtil {

	// Table information
	private static final String WATCHER_ENTITY_TABLE_NAME = "Users";
	private static final String WATCHER_TYPE = "user_type";
	private static final String WATCHER_PASSWORD_SECRET = "user_password_secret";
	private static final String WATCHER_CREATION_TIME = "user_creation_time";

	// Account information
	private static final String WATCHER_ACCOUNT_PROFILE_PHOTO_URL = "user_account_profile_url";
	private static final String WATCHER_ACCOUNT_ID = "user_account_id";
	private static final String WATCHER_ACCOUNT_PASSWORD = "user_account_password";

	// Personal information
	private static final String WATCHER_FIRSTNAME = "user_firstname";
	private static final String WATCHER_LASTNAME = "user_lastname";
	private static final String WATCHER_BIRTHDATE = "user_birthdate";
	private static final String WATCHER_GENDER = "user_gender";
	private static final String WATCHER_CELLPHONE = "user_cellphone";

	// Location information
	private static final String WATCHER_DISTRICT_AREA = "user_district_area";
	private static final String WATCHER_TOWNHALL_AREA = "user_townhall_area";
	private static final String WATCHER_NEIGHBORHOOD_AREA = "user_neighborhood_area";

	/**
	 * Just for class clarity.
	 */
	private WatcherDataUtil() {
	}

	public static final boolean isAccountIDTaken(DatastoreService datastore, Transaction txn, String accountID)
			throws IllegalStateException, TooManyResultsException {
		Filter accountIDFilter = new FilterPredicate(WATCHER_ACCOUNT_ID, FilterOperator.EQUAL, accountID);
		Query accountIDQuery = new Query(WATCHER_ENTITY_TABLE_NAME).setKeysOnly().setFilter(accountIDFilter);
		PreparedQuery q = datastore.prepare(accountIDQuery);
		Entity accountIDKey = q.asSingleEntity();
		if (accountIDKey == null) {
			return false;
		} else {
			return true;
		}
	}

	public static final boolean validWatcherDataFields(String accountPassword, String accountPasswordCheck,
			String watcherFirstname, String watcherLastname, String watcherBirthdate, String watcherGender,
			String watcherCellphone, String watcherDistrictArea, String watcherTownHallArea,
			String watcherNeighborhoodArea) {
		return true;
	}

	public static final String uploadAccountProfilePhoto(GcsService cloudstore, int cloudstoreBufferSize,
			String cloudstoreBucketName, String cloudstoreDefaultPhotoBucket, String accountID,
			byte[] accountProfilePhoto) {
		try {
			String photoMimeType = "";
			if ((photoMimeType = isProfilePhotoFormatValid(accountProfilePhoto)) != null) {
				GcsFileOptions gcsFileOptions = new GcsFileOptions.Builder().mimeType(photoMimeType).build();
				GcsFilename fileName = new GcsFilename(cloudstoreBucketName, accountID);
				GcsOutputChannel outputChannel = cloudstore.createOrReplace(fileName, gcsFileOptions);
				uploadToCloudstore(cloudstoreBufferSize, new ByteArrayInputStream(accountProfilePhoto),
						Channels.newOutputStream(outputChannel));
				return fileName.getBucketName() + "/" + fileName.getObjectName();
			}
			return cloudstoreDefaultPhotoBucket;
		} catch (IOException e) {
			return cloudstoreDefaultPhotoBucket;
		}
	}

	private static String isProfilePhotoFormatValid(byte[] accountProfilePhoto) {
		if (accountProfilePhoto != null) {
			return "jpeg";
		}
		return null;
	}

	private static void uploadToCloudstore(int cloudstoreBufferSize, InputStream accountPhotoProfile,
			OutputStream cloudstoreBucket) throws IOException {
		byte[] buffer = new byte[cloudstoreBufferSize];
		int bytesRead = accountPhotoProfile.read(buffer);
		while (bytesRead != -1) {
			cloudstoreBucket.write(buffer, 0, bytesRead);
			bytesRead = accountPhotoProfile.read(buffer);
		}
		accountPhotoProfile.close();
		cloudstoreBucket.close();
	}

	public static Entity createWatcherEntity(String accountID, String accountPassword, String watcherFirstname,
			String watcherLastname, String watcherBirthdate, String watcherGender, String watcherCellphone,
			String watcherDistrictArea, String watcherTownHallArea, String watcherNeighborhoodArea,
			String accountProfilePhotoURL, String watcherPasswordSecret, String watcherType) {
		Entity watcherEntity = new Entity(WATCHER_ENTITY_TABLE_NAME, accountID);
		watcherEntity.setIndexedProperty(WATCHER_TYPE, watcherType);
		watcherEntity.setIndexedProperty(WATCHER_PASSWORD_SECRET, watcherPasswordSecret);
		watcherEntity.setIndexedProperty(WATCHER_CREATION_TIME, System.currentTimeMillis());
		watcherEntity.setIndexedProperty(WATCHER_ACCOUNT_PROFILE_PHOTO_URL, accountProfilePhotoURL);
		watcherEntity.setIndexedProperty(WATCHER_ACCOUNT_ID, accountID);
		watcherEntity.setIndexedProperty(WATCHER_ACCOUNT_PASSWORD,
				DigestUtils.sha512Hex(accountPassword + watcherPasswordSecret));
		watcherEntity.setIndexedProperty(WATCHER_FIRSTNAME, watcherFirstname);
		watcherEntity.setIndexedProperty(WATCHER_LASTNAME, watcherLastname);
		watcherEntity.setIndexedProperty(WATCHER_BIRTHDATE, new LocalDate(watcherBirthdate).toDate());
		watcherEntity.setIndexedProperty(WATCHER_GENDER, watcherGender);
		watcherEntity.setIndexedProperty(WATCHER_CELLPHONE, (watcherCellphone != null) ? watcherCellphone : null);
		watcherEntity.setIndexedProperty(WATCHER_DISTRICT_AREA, watcherDistrictArea);
		watcherEntity.setIndexedProperty(WATCHER_TOWNHALL_AREA, watcherTownHallArea);
		watcherEntity.setIndexedProperty(WATCHER_NEIGHBORHOOD_AREA, watcherNeighborhoodArea);
		return watcherEntity;
	}

	public static void updateEntity(Entity watcherEntity, String accountPassword, String accountPasswordCheck,
			String watcherFirstname, String watcherLastname, String watcherBirthdate, String watcherGender,
			String watcherCellphone, String watcherDistrictArea, String watcherTownHallArea,
			String watcherNeighborhoodArea) {
		if (accountPassword != null) {
			String watcherPasswordSecret = (String) watcherEntity.getProperty(WATCHER_PASSWORD_SECRET);
			watcherEntity.setIndexedProperty(WATCHER_ACCOUNT_PASSWORD,
					DigestUtils.sha512Hex(accountPassword + watcherPasswordSecret));
		}
		if (watcherFirstname != null) {
			watcherEntity.setProperty(WATCHER_FIRSTNAME, watcherFirstname);
		}
		if (watcherLastname != null) {
			watcherEntity.setProperty(WATCHER_LASTNAME, watcherLastname);
		}
		if (watcherBirthdate != null) {
			watcherEntity.setProperty(WATCHER_BIRTHDATE, watcherBirthdate);
		}
		if (watcherGender != null) {
			watcherEntity.setProperty(WATCHER_GENDER, watcherGender);
		}
		if (watcherCellphone != null) {
			watcherEntity.setProperty(WATCHER_CELLPHONE, watcherCellphone);
		}
		if (watcherDistrictArea != null) {
			watcherEntity.setProperty(WATCHER_DISTRICT_AREA, watcherDistrictArea);
		}
		if (watcherTownHallArea != null) {
			watcherEntity.setProperty(WATCHER_TOWNHALL_AREA, watcherTownHallArea);
		}
		if (watcherNeighborhoodArea != null) {
			watcherEntity.setProperty(WATCHER_NEIGHBORHOOD_AREA, watcherNeighborhoodArea);
		}
	}

}