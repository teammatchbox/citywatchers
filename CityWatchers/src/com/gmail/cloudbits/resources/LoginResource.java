package com.gmail.cloudbits.resources;

import java.util.ConcurrentModificationException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.gmail.cloudbits.auth.AuthToken;
import com.gmail.cloudbits.auth.AuthTokenUtil;
import com.gmail.cloudbits.login.LoginData;
import com.gmail.cloudbits.login.LoginDataUtil;
import com.google.appengine.api.datastore.DatastoreFailureException;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/login")
public class LoginResource {

	private static final String USER_ENTITY_TABLE_NAME = "Users";
	private static final String USER_ACCOUNT_PASSWORD = "user_account_password";
	private static final String USER_PASSWORD_SECRET = "user_password_secret";
	private static final String USER_TYPE = "user_type";

	private static final DatastoreService DATASTORE = DatastoreServiceFactory.getDatastoreService();
	private static final Gson JSON_CONVERTER = new GsonBuilder().setPrettyPrinting().create();

	@POST
	@Path("/signin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLogin(@Context HttpServletRequest httpRequest, @Context HttpHeaders requestHeaders,
			LoginData userData) {
		if (userData != null) {
			String userEmailAddress = userData.getUserEmailAddress();
			String userPassword = userData.getUserPassword();
			if (LoginDataUtil.validLoginDataFields(userEmailAddress, userPassword)) {
				Transaction txn = null;
				try {
					txn = DATASTORE.beginTransaction(TransactionOptions.Builder.withXG(true)); // Allows multiple entity writing
					Entity userEntity = DATASTORE.get(KeyFactory.createKey(USER_ENTITY_TABLE_NAME, userEmailAddress));
					String userEntityPassword = (String) userEntity.getProperty(USER_ACCOUNT_PASSWORD);
					String userEntitySecret = (String) userEntity.getProperty(USER_PASSWORD_SECRET);
					String hashedPassword = DigestUtils.sha512Hex(userPassword + userEntitySecret);
					if (userEntityPassword.equals(hashedPassword)) {
						String userEntityType = (String) userEntity.getProperty(USER_TYPE);
						AuthToken userAuthToken = new AuthToken(userEmailAddress, userEntitySecret);
						Entity authTokenEntity = AuthTokenUtil.createAuthTokenEntity(userEmailAddress, userEntitySecret,
								userEntityType, userAuthToken);
						DATASTORE.put(txn, authTokenEntity);
						txn.commit();
						return Response.ok(JSON_CONVERTER.toJson(userAuthToken)).build();
					}
					txn.rollback();
					return Response.status(Status.FORBIDDEN).build();
				} catch (DatastoreFailureException e) {
					// Do nothing for now
				}
				catch (EntityNotFoundException e) {
					// Do nothing for now
				}
				catch (IllegalStateException e) {
					// Do nothing for now
				} catch (ConcurrentModificationException e) {
					// Do nothing for now
				} finally {
					if (txn != null && txn.isActive()) {
						txn.rollback();
					}
				}
			}
		}
		return Response.status(Status.FORBIDDEN).build();
	}

}