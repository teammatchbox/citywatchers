package com.gmail.cloudbits.resources;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.gmail.cloudbits.auth.AuthToken;
import com.gmail.cloudbits.auth.AuthTokenUtil;
import com.gmail.cloudbits.report.ReportData;
import com.gmail.cloudbits.report.ReportDataUtil;
import com.gmail.cloudbits.report.ReportFilter;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/report")
public class ReportResource {

	private static final String REPORT_ENTITY_TABLE_NAME = "Reports";
	private static final String TOKEN_OWNER_PROPERTY = "token_owner";
	private static final String REPORT_CURRENT_STATUS = "report_current_status";
	private static final String ANONYMOUS_WATCHER = "Anonymous";
	private static final String SUBMITTED_REPORT = "Submitted";
	private static final String OPENED_REPORT = "Opened";
	private static final String CLOSED_REPORT = "Closed";

	private static final DatastoreService DATASTORE = DatastoreServiceFactory.getDatastoreService();
	private static final Gson JSON_CONVERTER = new GsonBuilder().setPrettyPrinting().create();

	public ReportResource() {
	}

	@POST
	@Path("/new")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doReport(ReportData data, @Context HttpServletRequest request, @Context HttpHeaders headers) {
		AuthToken userAuthToken = data.getAuthToken();
		Transaction txn = null;
		try {
			if (userAuthToken != null) {
				String userAuthTokenID = userAuthToken.getTokenID();
				long userAuthTokenCreationTime = userAuthToken.getTokenCreationTime();
				long userAuthTokenExpirationTime = userAuthToken.getTokenExpirationTime();
				String userAuthTokenHash = userAuthToken.getTokenHash();
				if (AuthTokenUtil.validAuthTokenFields(userAuthTokenID, userAuthTokenCreationTime,
						userAuthTokenExpirationTime, userAuthTokenHash)) {
					txn = DATASTORE.beginTransaction(TransactionOptions.Builder.withXG(true));
					Entity userAuthTokenEntity = AuthTokenUtil.getAuthTokenEntityFromDatastore(DATASTORE, txn,
							userAuthTokenID, userAuthTokenExpirationTime, userAuthTokenHash);
					if (userAuthTokenEntity != null) {
						String authTokenOwner = (String) userAuthTokenEntity.getProperty(TOKEN_OWNER_PROPERTY);
						String reportAddress = data.getReportAddress();
						String reportPriority = data.getReportPriority();
						String reportType = data.getReportType();
						String reportDescription = data.getReportDescription();
						Entity reportEntity = ReportDataUtil.createReportEntity(reportAddress, reportPriority,
								reportType, reportDescription, authTokenOwner, SUBMITTED_REPORT);
						DATASTORE.put(reportEntity);
						txn.commit();
						return Response.ok().build();
					}
				}
				return Response.status(Status.BAD_REQUEST).build();
			} else { // Anonymous
				String reportAddress = data.getReportAddress();
				String reportPriority = data.getReportPriority();
				String reportType = data.getReportType();
				String reportDescription = data.getReportDescription();
				txn = DATASTORE.beginTransaction();
				Entity reportEntity = ReportDataUtil.createReportEntity(reportAddress, reportPriority, reportType,
						reportDescription, ANONYMOUS_WATCHER, SUBMITTED_REPORT);
				DATASTORE.put(reportEntity);
				txn.commit();
				return Response.ok().build();
			}
		} catch (Exception e) {
			// Do nothing for now
		} finally {
			if (txn != null && txn.isActive()) {
				// GAE error
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}

	@POST
	@Path("/get")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getReport(ReportFilter searchParams) {
		String filter = "";
		if (searchParams.isGetSubmitted()) {
			filter = SUBMITTED_REPORT;
		} else if (searchParams.isGetOpened()) {
			filter = OPENED_REPORT;
		} else if (searchParams.isGetClosed()) {
			filter = CLOSED_REPORT;
		}
		Filter reportFilter = new FilterPredicate(REPORT_CURRENT_STATUS, FilterOperator.EQUAL, filter);
		Query reportQuery = new Query(REPORT_ENTITY_TABLE_NAME).setFilter(reportFilter);
		Iterator<Entity> datastoreEntities = DATASTORE.prepare(reportQuery)
				.asIterator(FetchOptions.Builder.withLimit(50));
		List<Entity> reportEntities = new ArrayList<Entity>();
		while (datastoreEntities.hasNext()) {
			reportEntities.add(datastoreEntities.next());
		}
		return Response.ok(JSON_CONVERTER.toJson(reportEntities)).build();
	}

}