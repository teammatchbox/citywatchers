package com.gmail.cloudbits.resources;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.gmail.cloudbits.auth.AuthToken;
import com.gmail.cloudbits.auth.AuthTokenUtil;
import com.gmail.cloudbits.watcher.WatcherData;
import com.gmail.cloudbits.watcher.WatcherDataUtil;
import com.google.appengine.api.datastore.DatastoreFailureException;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/user")
public class ProfileResource {

	private static final String TOKEN_OWNER_PROPERTY = "token_owner";
	private static final String USERS_ENTITY_TABLE_NAME = "Users";

	private static final String WATCHER_ACCOUNT_PROFILE_PHOTO_URL = "user_account_profile_url";
	private static final String WATCHER_ACCOUNT_ID = "user_account_id";

	// Personal information
	private static final String WATCHER_FIRSTNAME = "user_firstname";
	private static final String WATCHER_LASTNAME = "user_lastname";
	private static final String WATCHER_BIRTHDATE = "user_birthdate";
	private static final String WATCHER_GENDER = "user_gender";
	private static final String WATCHER_CELLPHONE = "user_cellphone";

	// Location information
	private static final String WATCHER_DISTRICT_AREA = "user_district_area";
	private static final String WATCHER_TOWNHALL_AREA = "user_townhall_area";
	private static final String WATCHER_NEIGHBORHOOD_AREA = "user_neighborhood_area";

	public static final boolean SERVE_USING_BLOBSTORE_API = false;

	/**
	 * This is where backoff parameters are configured. Here it is aggressively
	 * retrying with backoff, up to 10 times but taking no more that 15 seconds
	 * total to do so.
	 */
	// private final GcsService gcsService =
	// GcsServiceFactory.createGcsService(new RetryParams.Builder()
	// .initialRetryDelayMillis(10).retryMaxAttempts(10).totalRetryPeriodMillis(15000).build());

	/**
	 * Used below to determine the size of chucks to read in. Should be > 1kb
	 * and < 10MB
	 */
	// private static final int BUFFER_SIZE = 2 * 1024 * 1024;
	private static final DatastoreService DATASTORE = DatastoreServiceFactory.getDatastoreService();
	private static final Gson JSON_CONVERTER = new GsonBuilder().setPrettyPrinting().create();

	/**
	 * Just for class clarity.
	 */
	public ProfileResource() {
	}

	@POST
	@Path("/watcher/profile")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public final Response getWatcherProfile(AuthToken userAuthToken) {
		if (userAuthToken != null) {
			String userAuthTokenID = userAuthToken.getTokenID();
			long userAuthTokenCreationTime = userAuthToken.getTokenCreationTime();
			long userAuthTokenExpirationTime = userAuthToken.getTokenExpirationTime();
			String userAuthTokenHash = userAuthToken.getTokenHash();
			if (AuthTokenUtil.validAuthTokenFields(userAuthTokenID, userAuthTokenCreationTime,
					userAuthTokenExpirationTime, userAuthTokenHash)) {
				Transaction txn = null;
				try {
					txn = DATASTORE.beginTransaction(TransactionOptions.Builder.withXG(true));
					Entity userAuthTokenEntity = AuthTokenUtil.getAuthTokenEntityFromDatastore(DATASTORE, txn,
							userAuthTokenID, userAuthTokenExpirationTime, userAuthTokenHash);
					if (userAuthTokenEntity != null) {
						String authTokenOwner = (String) userAuthTokenEntity.getProperty(TOKEN_OWNER_PROPERTY);
						Entity userEntity = DATASTORE
								.get(KeyFactory.createKey(USERS_ENTITY_TABLE_NAME, authTokenOwner));
						Map<String, Object> userProfile = new HashMap<String, Object>();
						byte[] accountProfilePhoto = getPhotoFromCloudstore(
								((String) userEntity.getProperty(WATCHER_ACCOUNT_PROFILE_PHOTO_URL)), userProfile);
						userProfile.put(WATCHER_ACCOUNT_PROFILE_PHOTO_URL, accountProfilePhoto);
						userProfile.put(WATCHER_ACCOUNT_ID, (String) userEntity.getProperty(WATCHER_ACCOUNT_ID));
						userProfile.put(WATCHER_FIRSTNAME, (String) userEntity.getProperty(WATCHER_FIRSTNAME));
						userProfile.put(WATCHER_LASTNAME, (String) userEntity.getProperty(WATCHER_LASTNAME));
						userProfile.put(WATCHER_BIRTHDATE,
								((Date) userEntity.getProperty(WATCHER_BIRTHDATE)).toString());
						userProfile.put(WATCHER_GENDER, (String) userEntity.getProperty(WATCHER_GENDER));
						userProfile.put(WATCHER_CELLPHONE, (String) userEntity.getProperty(WATCHER_CELLPHONE));
						userProfile.put(WATCHER_DISTRICT_AREA, (String) userEntity.getProperty(WATCHER_DISTRICT_AREA));
						userProfile.put(WATCHER_TOWNHALL_AREA, (String) userEntity.getProperty(WATCHER_TOWNHALL_AREA));
						userProfile.put(WATCHER_NEIGHBORHOOD_AREA,
								(String) userEntity.getProperty(WATCHER_NEIGHBORHOOD_AREA));
						txn.rollbackAsync();
						return Response.ok(JSON_CONVERTER.toJson(userProfile)).build();
					}
					txn.rollback();
					return Response.status(Status.FORBIDDEN).build();
				} catch (EntityNotFoundException e) {

				} catch (DatastoreFailureException e) {

				} finally {
					if (txn != null && txn.isActive()) {
						// GAE error
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).build();
					}
				}
			}
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

	private byte[] getPhotoFromCloudstore(String photoBucketURL, Map<String, Object> userProfile) {
		/*
		 * GcsFilename fileName = getFileName(photoBucketURL); GcsFileMetadata
		 * metadata = gcsService.getMetadata(fileName); byte[] photoBuffer = new
		 * byte[(int)metadata.getLength()]; String contentType =
		 * metadata.getOptions().getContentEncoding(); GcsInputChannel
		 * readChannel = gcsService.openPrefetchingReadChannel(fileName, 0,
		 * BUFFER_SIZE); userProfile.put("photo_content_type", contentType); //
		 * copy(Channels.newInputStream(readChannel), resp.getOutputStream());
		 * return photoBuffer;
		 */
		return null;
	}

	/*
	 * private GcsFilename getFileName(String photoBucketURL) { String[] splits
	 * = photoBucketURL.split("/", 4); if (!splits[0].equals("") ||
	 * !splits[1].equals("gcs")) { throw new
	 * IllegalArgumentException("The URL is not formed as expected. " +
	 * "Expecting /gcs/<bucket>/<object>"); } return new GcsFilename(splits[2],
	 * splits[3]); }
	 */

	@POST
	@Path("/watcher/profile/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public final Response updateWatcherProfile(WatcherData updatededData) {
		if (updatededData != null) {
			Transaction txn = null;
			try {
				txn = DATASTORE.beginTransaction();
				Entity watcherEntity = DATASTORE
						.get(KeyFactory.createKey(USERS_ENTITY_TABLE_NAME, updatededData.getAccountID()));
				String accountPassword = updatededData.getAccountPassword();
				String accountPasswordCheck = updatededData.getAccountPasswordCheck();
				String watcherFirstname = updatededData.getWatcherFirstname();
				String watcherLastname = updatededData.getWatcherLastname();
				String watcherBirthdate = updatededData.getWatcherBirthdate();
				String watcherGender = updatededData.getWatcherGender();
				String watcherCellphone = updatededData.getWatcherCellphone();
				String watcherDistrictArea = updatededData.getWatcherDistrictArea();
				String watcherTownHallArea = updatededData.getWatcherTownHallArea();
				String watcherNeighborhoodArea = updatededData.getWatcherNeighborhoodArea();
				WatcherDataUtil.updateEntity(watcherEntity, accountPassword, accountPasswordCheck, watcherFirstname,
						watcherLastname, watcherBirthdate, watcherGender, watcherCellphone, watcherDistrictArea,
						watcherTownHallArea, watcherNeighborhoodArea);
				DATASTORE.put(watcherEntity);
				txn.commit();
			} catch (Exception e) {
				// Do nothing for now
			} finally {
				if (txn != null && txn.isActive()) {
					// GAE error
					txn.rollback();
					return Response.status(Status.INTERNAL_SERVER_ERROR).build();
				}
			}
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

	@POST
	@Path("/watcher/profile/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	public final Response deleteWatcherProfile(AuthToken userAuthToken) {
		if (userAuthToken != null) {
			String userAuthTokenID = userAuthToken.getTokenID();
			long userAuthTokenCreationTime = userAuthToken.getTokenCreationTime();
			long userAuthTokenExpirationTime = userAuthToken.getTokenExpirationTime();
			String userAuthTokenHash = userAuthToken.getTokenHash();
			if (AuthTokenUtil.validAuthTokenFields(userAuthTokenID, userAuthTokenCreationTime,
					userAuthTokenExpirationTime, userAuthTokenHash)) {
				Transaction txn = null;
				try {
					txn = DATASTORE.beginTransaction(TransactionOptions.Builder.withXG(true));
					Entity userAuthTokenEntity = AuthTokenUtil.getAuthTokenEntityFromDatastore(DATASTORE, txn,
							userAuthTokenID, userAuthTokenExpirationTime, userAuthTokenHash);
					if (userAuthTokenEntity != null) {
						String authTokenOwner = (String) userAuthTokenEntity.getProperty(TOKEN_OWNER_PROPERTY);
						DATASTORE.delete(KeyFactory.createKey(USERS_ENTITY_TABLE_NAME, authTokenOwner));
						txn.commit();
						return Response.ok().build();
					}
					txn.rollback();
					return Response.status(Status.FORBIDDEN).build();
				} catch (EntityNotFoundException e) {

				} catch (DatastoreFailureException e) {

				} finally {
					if (txn != null && txn.isActive()) {
						// GAE error
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).build();
					}
				}
			}
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

}