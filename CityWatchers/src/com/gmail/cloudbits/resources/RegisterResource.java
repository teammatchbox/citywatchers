package com.gmail.cloudbits.resources;

import java.util.Arrays;
import java.util.UUID;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.gmail.cloudbits.auth.AuthToken;
import com.gmail.cloudbits.auth.AuthTokenUtil;
import com.gmail.cloudbits.watcher.WatcherData;
import com.gmail.cloudbits.watcher.WatcherDataUtil;
import com.google.appengine.api.datastore.DatastoreFailureException;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery.TooManyResultsException;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/register")
public final class RegisterResource {

	// GAE cloudstore bucket name and buffer size.
	private static final String CLOUDSTORE_PATH_NAME = "/gcs/";
	private static final String CLOUDSTORE_DEFAULT_PROFILE_PHOTO_BUCKET = "city-watchers-167210.appspot.com/defaultProfilePhoto.jpeg";
	private static final String CLOUDSTORE_BUCKET_NAME = "city-watchers-167210.appspot.com";
	private static final int CLOUDSTORE_BUFFER_SIZE = 2 * 1024 * 1024;

	// GAE access objects.
	private static final DatastoreService DATASTORE = DatastoreServiceFactory.getDatastoreService();
	private static final GcsService CLOUDSTORE = GcsServiceFactory.createGcsService(new RetryParams.Builder()
			.initialRetryDelayMillis(10).retryMaxAttempts(10).totalRetryPeriodMillis(15000).build());

	// Class static objects.
	private static final String WATCHER_USER = "Watcher";
	// private static final String FIXER_USER = "Fixer";
	// private static final String APPROVER_USER = "Approver";
	// private static final String USER_TYPE_PROPERTY = "user_type";
	private static final Logger LOG = Logger.getLogger(RegisterResource.class.getName());
	private static final Gson JSON_CONVERTER = new GsonBuilder().setPrettyPrinting().create();

	/**
	 * Just for class clarity.
	 */
	public RegisterResource() {
	}

	@POST
	@Path("/fixer/signup")
	@Consumes(MediaType.APPLICATION_JSON)
	public final Response doApproverRegistration() {
		return null;
	}

	/*@POST
	@Path("/manager/signup")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public final Response doFixerRegistration(AuthToken approverToken, FixerData fixerData) {
		if (approverToken != null && fixerData != null) {
			String userAuthTokenID = approverToken.getTokenID();
			long userAuthTokenCreationTime = approverToken.getTokenCreationTime();
			long userAuthTokenExpirationTime = approverToken.getTokenExpirationTime();
			String userAuthTokenHash = approverToken.getTokenHash();
			if (AuthTokenUtil.validAuthTokenFields(userAuthTokenID, userAuthTokenCreationTime,
					userAuthTokenExpirationTime, userAuthTokenHash)) {
				Transaction txn = null;
				try {
					txn = DATASTORE.beginTransaction(TransactionOptions.Builder.withXG(true));
					Entity approverAuthTokenEntity = AuthTokenUtil.getAuthTokenEntityFromDatastore(DATASTORE, txn,
							userAuthTokenID, userAuthTokenExpirationTime, userAuthTokenHash);
					if (approverAuthTokenEntity != null) {
						String approverUserType = (String) approverAuthTokenEntity.getProperty(USER_TYPE_PROPERTY);
						if (approverUserType.equals(APPROVER_USER)) {
							String accountID = fixerData.getAccountID();
							if (!FixerDataUtil.isAccountIDTaken(DATASTORE, txn, accountID)) {
								String fixerAccountPassword = fixerData.getAccountPassword();
								String fixerAccountPasswordCheck = fixerData.getAccountPasswordCheck();
								String fixerEmployeeEntity = fixerData.getFixerEmployeeEntity();
								String fixerEmployerEntity = fixerData.getFixerEmployerEntity();
								String fixerReportSuperType = fixerData.getFixerReportSuperType();
								String fixerReportSubType = fixerData.getFixerReportSubType();
								String fixerAssignedDistrictArea = fixerData.getFixerDistrictArea();
								String fixerAssignedTownHallArea = fixerData.getFixerTownHallArea();
								String fixerAssignedNeighborhoodArea = fixerData.getFixerNeighborhoodArea();
								if (FixerDataUtil.validFixerDataFields(fixerAccountPassword, fixerAccountPasswordCheck,
										fixerEmployeeEntity, fixerReportSuperType, fixerReportSubType,
										fixerAssignedDistrictArea, fixerAssignedTownHallArea,
										fixerAssignedNeighborhoodArea)) {
									String fixerPasswordSecret = UUID.randomUUID().toString();
									Entity fixerEntity = FixerDataUtil.createFixerEntity(accountID, FIXER_USER,
											fixerPasswordSecret, fixerAccountPassword, fixerEmployeeEntity,
											fixerEmployerEntity, fixerReportSuperType, fixerReportSubType,
											fixerAssignedDistrictArea, fixerAssignedTownHallArea,
											fixerAssignedNeighborhoodArea);
									AuthToken userAuthToken = new AuthToken(accountID, fixerPasswordSecret);
									Entity authTokenEntity = AuthTokenUtil.createAuthTokenEntity(accountID,
											fixerPasswordSecret, WATCHER_USER, userAuthToken);
									DATASTORE.put(txn, Arrays.asList(fixerEntity, authTokenEntity));
									txn.commit();
									return Response.ok().entity(JSON_CONVERTER.toJson(userAuthToken)).build();
								}

							}
						}
					}
					txn.rollback();
					return Response.status(Status.FORBIDDEN).build();
				} catch (DatastoreFailureException e) {
					LOG.fine("doWatcherRegistration - DatastoreFailureException");
				} catch (IllegalStateException e) {
					LOG.fine("doWatcherRegistration - IllegalStateException");
				} catch (TooManyResultsException e) {
					LOG.fine("doWatcherRegistration - TooManyResultsException");
				} catch (EntityNotFoundException e) {
					LOG.fine("doWatcherRegistration - EntityNotFoundException");
				} finally {
					if (txn != null && txn.isActive()) {
						// GAE error
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).build();
					}
				}
			}
		}
		return Response.status(Status.FORBIDDEN).build();
	}*/

	@POST
	@Path("/watcher/signup")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public final Response doWatcherRegistration(WatcherData watcherData) {
		if (watcherData != null) {
			Transaction txn = null;
			try {
				txn = DATASTORE.beginTransaction(TransactionOptions.Builder.withXG(true)); // Allows
				// multiple
				// entity
				// writing
				String accountID = watcherData.getAccountID();
				if (!WatcherDataUtil.isAccountIDTaken(DATASTORE, txn, accountID)) {
					String accountPassword = watcherData.getAccountPassword();
					String accountPasswordCheck = watcherData.getAccountPasswordCheck();
					String watcherFirstname = watcherData.getWatcherFirstname();
					String watcherLastname = watcherData.getWatcherLastname();
					String watcherBirthdate = watcherData.getWatcherBirthdate();
					String watcherGender = watcherData.getWatcherGender();
					String watcherCellphone = watcherData.getWatcherCellphone();
					String watcherDistrictArea = watcherData.getWatcherDistrictArea();
					String watcherTownHallArea = watcherData.getWatcherTownHallArea();
					String watcherNeighborhoodArea = watcherData.getWatcherNeighborhoodArea();
					if (WatcherDataUtil.validWatcherDataFields(accountPassword, accountPasswordCheck, watcherFirstname,
							watcherLastname, watcherBirthdate, watcherGender, watcherCellphone, watcherDistrictArea,
							watcherTownHallArea, watcherNeighborhoodArea)) {
						byte[] accountProfilePhoto = watcherData.getAccountProfilePhoto();
						String accountProfilePhotoURL = CLOUDSTORE_PATH_NAME + WatcherDataUtil
								.uploadAccountProfilePhoto(CLOUDSTORE, CLOUDSTORE_BUFFER_SIZE, CLOUDSTORE_BUCKET_NAME,
										CLOUDSTORE_DEFAULT_PROFILE_PHOTO_BUCKET, accountID, accountProfilePhoto);
						String watcherPasswordSecret = UUID.randomUUID().toString();
						Entity watcherEntity = WatcherDataUtil.createWatcherEntity(accountID, accountPassword,
								watcherFirstname, watcherLastname, watcherBirthdate, watcherGender, watcherCellphone,
								watcherDistrictArea, watcherTownHallArea, watcherNeighborhoodArea,
								accountProfilePhotoURL, watcherPasswordSecret, WATCHER_USER);
						AuthToken userAuthToken = new AuthToken(accountID, watcherPasswordSecret);
						Entity authTokenEntity = AuthTokenUtil.createAuthTokenEntity(accountID, watcherPasswordSecret,
								WATCHER_USER, userAuthToken);
						DATASTORE.put(txn, Arrays.asList(watcherEntity, authTokenEntity));
						txn.commit();
						return Response.ok().entity(JSON_CONVERTER.toJson(userAuthToken)).build();
					}
				}
				// Invalid data sent by the client
				txn.rollback();
				return Response.status(Status.BAD_REQUEST).build();
			} catch (DatastoreFailureException e) {
				LOG.fine("doWatcherRegistration - DatastoreFailureException");
			} catch (IllegalStateException e) {
				LOG.fine("doWatcherRegistration - IllegalStateException");
			} catch (TooManyResultsException e) {
				LOG.fine("doWatcherRegistration - TooManyResultsException");
			} finally {
				if (txn != null && txn.isActive()) {
					// GAE error
					txn.rollback();
					return Response.status(Status.INTERNAL_SERVER_ERROR).build();
				}
			}
		}
		// Invalid data sent by the client
		return Response.status(Status.BAD_REQUEST).build();
	}

}