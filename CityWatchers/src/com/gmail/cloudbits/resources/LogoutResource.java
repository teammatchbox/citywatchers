package com.gmail.cloudbits.resources;

import java.util.ConcurrentModificationException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.gmail.cloudbits.auth.AuthToken;
import com.gmail.cloudbits.auth.AuthTokenUtil;
import com.google.appengine.api.datastore.DatastoreFailureException;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Transaction;

@Path("/logout")
public class LogoutResource {

	private static final DatastoreService DATASTORE = DatastoreServiceFactory.getDatastoreService();

	public LogoutResource() {}

	@POST
	@Path("/signout")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doLogout(AuthToken userAuthToken) {
		if (userAuthToken != null) {
			Transaction txn = null;
			String userAuthTokenID = userAuthToken.getTokenID();
			long userAuthTokenCreationTime = userAuthToken.getTokenCreationTime();
			long userAuthTokenExpirationTime = userAuthToken.getTokenExpirationTime();
			String userAuthTokenHash = userAuthToken.getTokenHash();
			if (AuthTokenUtil.validAuthTokenFields(userAuthTokenID, userAuthTokenCreationTime,
					userAuthTokenExpirationTime, userAuthTokenHash)) {
				try {
					txn = DATASTORE.beginTransaction();
					Entity userAuthTokenEntity = AuthTokenUtil.getAuthTokenEntityFromDatastore(DATASTORE, txn,
							userAuthTokenID, userAuthTokenExpirationTime, userAuthTokenHash);
					if (userAuthTokenEntity != null) {
						AuthTokenUtil.markAuthTokenAsExpired(userAuthTokenEntity);
						DATASTORE.put(txn, userAuthTokenEntity);
						txn.commit();
						return Response.ok().build();
					}
					txn.rollback();
					return Response.status(Status.BAD_REQUEST).build();
				} catch (DatastoreFailureException e) {
					// Do nothing for now
				} catch (EntityNotFoundException e) {
					// Do nothing for now
				} catch (IllegalStateException e) {
					// Do nothing for now
				} catch (ConcurrentModificationException e) {
					// Do nothing for now
				} finally {
					if (txn != null && txn.isActive()) {
						txn.rollback();
					}
				}
			}
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

}