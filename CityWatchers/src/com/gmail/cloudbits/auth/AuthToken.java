package com.gmail.cloudbits.auth;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * {@code AuthToken} is a class, that contains methods to create authentication
 * {@code tokens}, and perform basic retrieving operations on the same {@code token}.
 * 
 * @author CloudBits
 */
public class AuthToken {

	private static final long TOKEN_TIMEOUT = 1 * 1000 * 60 * 60; // One hour limit before timeout;

	private String tokenID; // Generate a hash to let the server know that this token belongs to a valid user.
	private long tokenCreationTime;
	private long tokenExpirationTime;
	private String tokenHash; // Generate a hash to let the server know this token was created by him.

	/**
	 * Empty constructor to handle error situations.
	 */
	public AuthToken() {}

	/**
	 * Creates a authentication {@code token} for the provided {@code owner}.
	 * 
	 * @param tokenOwnerEmailAddress
	 *            {@code token} owner.
	 * @param tokenOwnerSecret
	 *            {@code token} owner secret.
	 */
	public AuthToken(String tokenOwnerEmailAddress, String tokenOwnerSecret) {
		long creationTime = System.currentTimeMillis();
		long expirationTime = creationTime + TOKEN_TIMEOUT;
		this.tokenID = DigestUtils.sha512Hex(tokenOwnerEmailAddress + tokenOwnerSecret + creationTime);
		this.tokenCreationTime = creationTime;
		this.tokenExpirationTime = expirationTime;
		this.tokenHash = DigestUtils.sha512Hex(tokenOwnerSecret + creationTime);
	}

	/**
	 * Returns the id of this {@code token}.
	 * 
	 * @return the id of this {@code token}.
	 */
	public String getTokenID() {
		return this.tokenID;
	}

	/**
	 * Returns the creation time in UNIX format of this {@code token}.
	 * 
	 * @return the creation time in UNIX format of this {@code token}.
	 */
	public long getTokenCreationTime() {
		return this.tokenCreationTime;
	}

	/**
	 * Returns the expiration time in UNIX format of this {@code token}.
	 * 
	 * @return the expiration time in UNIX format of this {@code token}.
	 */
	public long getTokenExpirationTime() {
		return this.tokenExpirationTime;
	}

	/**
	 * Returns the hash of this {@code token}.
	 * 
	 * @return the hash of this {@code token}.
	 */
	public String getTokenHash() {
		return this.tokenHash;
	}

}