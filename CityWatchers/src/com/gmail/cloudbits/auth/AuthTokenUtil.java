package com.gmail.cloudbits.auth;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreFailureException;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

/**
 * {@code AuthTokenUtil} is an util class, that contains methods to create
 * {@code token} entities, perform basic validation and retrieving operations on
 * a {@code token} entity.
 * 
 * @author CloudBits
 */
public class AuthTokenUtil {

	private static final String TOKEN_ENTITY_NAME_PROPERTY = "AuthToken";
	private static final String TOKEN_OWNER_PROPERTY = "token_owner";
	private static final String TOKEN_OWNER_SECRET_PROPERTY = "token_owner_secret";
	private static final String TOKEN_OWNER_TYPE_PROPERTY = "token_owner_type";
	private static final String TOKEN_ID_PROPERTY = "token_id";
	private static final String TOKEN_CREATION_TIME_PROPERTY = "token_creation_time";
	private static final String TOKEN_EXPIRATION_TIME_PROPERTY = "token_expiration_time";
	private static final String TOKEN_HASH_PROPERTY = "token_hash";
	private static final String TOKEN_EXPIRED_PROPERTY = "token_expired";

	public AuthTokenUtil() {
	}

	public static Entity createAuthTokenEntity(String userEmailAddress, String userSecret, String userType,
			AuthToken userAuthToken) {
		String userAuthTokenID = userAuthToken.getTokenID();
		Entity authTokenEntity = new Entity(TOKEN_ENTITY_NAME_PROPERTY, userAuthTokenID);
		authTokenEntity.setUnindexedProperty(TOKEN_OWNER_PROPERTY, userEmailAddress);
		authTokenEntity.setUnindexedProperty(TOKEN_OWNER_SECRET_PROPERTY, userSecret);
		authTokenEntity.setUnindexedProperty(TOKEN_OWNER_TYPE_PROPERTY, userType);
		authTokenEntity.setIndexedProperty(TOKEN_ID_PROPERTY, userAuthTokenID);
		authTokenEntity.setUnindexedProperty(TOKEN_CREATION_TIME_PROPERTY, userAuthToken.getTokenCreationTime());
		authTokenEntity.setIndexedProperty(TOKEN_EXPIRATION_TIME_PROPERTY, userAuthToken.getTokenExpirationTime());
		authTokenEntity.setUnindexedProperty(TOKEN_HASH_PROPERTY, userAuthToken.getTokenHash());
		authTokenEntity.setIndexedProperty(TOKEN_EXPIRED_PROPERTY, false);
		return authTokenEntity;
	}

	public static boolean validAuthTokenFields(String userAuthTokenID, long userAuthTokenCreationTime,
			long userAuthTokenExpirationTime, String userAuthTokenHash) {
		return true;
	}

	public static Entity getAuthTokenEntityFromDatastore(DatastoreService datastore, Transaction txn,
			String userAuthTokenID, long userAuthTokenExpirationTime, String userAuthTokenHash)
					throws EntityNotFoundException, DatastoreFailureException {
		Entity authTokenEntity = datastore.get(KeyFactory.createKey(TOKEN_ENTITY_NAME_PROPERTY, userAuthTokenID));
		String tokenOwner = (String) authTokenEntity.getProperty(TOKEN_OWNER_PROPERTY);
		String tokenOwnerSecret = (String) authTokenEntity.getProperty(TOKEN_OWNER_SECRET_PROPERTY);
		long tokenCreationTime = (long) authTokenEntity.getProperty(TOKEN_CREATION_TIME_PROPERTY);
		boolean tokenExpired = (boolean) authTokenEntity.getProperty(TOKEN_EXPIRED_PROPERTY);
		String hashedAuthTokenID = DigestUtils.sha512Hex(tokenOwner + tokenOwnerSecret + tokenCreationTime);
		String hashedAuthTokenHash = DigestUtils.sha512Hex(tokenOwnerSecret + tokenCreationTime);
		if (hashedAuthTokenID.equals(userAuthTokenID) && hashedAuthTokenHash.equals(userAuthTokenHash)) {
			if (!tokenExpired && userAuthTokenExpirationTime > System.currentTimeMillis()) {
				return authTokenEntity;
			}
		}
		return null;
	}

	public static void markAuthTokenAsExpired(Entity userAuthTokenEntity) {
		userAuthTokenEntity.setProperty(TOKEN_EXPIRED_PROPERTY, true);

	}

}