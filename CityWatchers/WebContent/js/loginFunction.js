captureData = function(event) {
	var data = $jquery('form[name="login"]').jsonify();
    console.log(data);
    $.ajax({
        type: "POST",
        url: "https://city-watchers-167210.appspot.com/rest/login/signin",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
        	 if(response) {
                 alert("User registered with username: " + response);
                  //Store token id for later use in localStorage
                  localStorage.setItem('tokenID', response.tokenID);
                  localStorage.setItem('tokenCreationTime', response.tokenCreationTime);
                  localStorage.setItem('tokenExpirationTime', response.tokenExpirationTime);
                  localStorage.setItem('tokenHash', response.tokenHash);
             }
            else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Error: "+ response.status);
        },
        data: JSON.stringify(data)
    });

    event.preventDefault();
};

window.onload = function() {
    var frms = $('form[name="login"]');     //var frms = document.getElementsByName("login");
    frms[0].onsubmit = captureData;
}