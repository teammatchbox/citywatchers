captureData = function(event) {
	var data = $jquery('form[name="ver_ocorrência"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "https://city-watchers-167210.appspot.com/rest/report/get",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		dataType: "json",
		success: function(response) {
			if(response) {
				for (var i = 0; i < Object.keys(response).length; i++) {
					var report = response[i].propertyMap;
					$("#resultarea").append(
							"<div>" +
							"<br>" + "Report " + (i + 1) + ":" + "</br>" +
							"<br>" + "Report type:" + report.report_type + "</br>" +
							"<br>" + "Report priority: " + report.report_priority + "</br>" +
							"<br>" + "Report status: " + report.report_current_status + "</br>" +
							"<br>" + "Report address: " + report.report_address + "</br>" +
							"<br>" + "Report description: " + report.report_description + "</br>" +
							"<br>" + "Watcher: " + report.report_submitted_by + "</br>" +
							"<br>" + "Report creation time: " + report.report_creation_time + "</br>" +
							"</div>"
					);
				}
			}
			else {
				alert("No response");
			}
		},
		error: function(response) {
			alert("Error: "+ response.status);
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

window.onload = function() {
	var frms = $('form[name="ver_ocorrência"]');     //var frms = document.getElementsByName("login");
	frms[0].onsubmit = captureData;
}