captureData = function(event) {
	var data = $jquery('form[name="registerReportLogado"]').jsonify();
    console.log(data);
    $.ajax({
        type: "POST",
        url: "https://city-watchers-167210.appspot.com/rest/report/new",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
            if(response) {
                alert("Got token with id: SUCESS");
            }
            else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Error: "+ response.status);
        },
        data: JSON.stringify(data)
    });

    event.preventDefault();
};

window.onload = function() {
    var frms = $('form[name="registerReportLogado"]');     //var frms = document.getElementsByName("login");
    frms[0].onsubmit = captureData;
}